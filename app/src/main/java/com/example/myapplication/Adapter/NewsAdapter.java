package com.example.myapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.Title;
import com.example.myapplication.WebActivity;

import java.util.List;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private List<Title> list;
    private Context context;
    private Title title;

    static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_title;
        private TextView tv_data;
        private ImageView title_pic;
        private ImageView title_pic2;
        private ImageView title_pic3;
        private TextView tv_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_data=itemView.findViewById(R.id.tv_data);
            tv_title=itemView.findViewById(R.id.tv_title);
            title_pic=itemView.findViewById(R.id.title_pic);
            tv_name=itemView.findViewById(R.id.tv_name);
        }
    }
    public NewsAdapter(Context context,List<Title> list){
        this.context=context;
        this.list=list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_1,viewGroup,false);
        final ViewHolder viewHolder=new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position=viewHolder.getAdapterPosition();
                Log.d("aaa", String.valueOf(position));
                Title title=list.get(position);
                Intent intent=new Intent(context,WebActivity.class);
                intent.putExtra("data",title.getUrl());
                context.startActivity(intent);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        title=list.get(i);
        viewHolder.tv_title.setText(title.getTitle());
        viewHolder.tv_data.setText(title.getData());
        viewHolder.tv_name.setText(title.getAuthor_name());
        Glide.with(context).load(title.getPciurle()).into(viewHolder.title_pic);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
