package com.example.myapplication.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.myapplication.R;
import com.example.myapplication.gson.Constellation;
import com.example.myapplication.unti.HttpUtil;
import com.example.myapplication.unti.Utility;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;



public class ConstellationFragment extends Fragment {
    private String start[] = {"白羊座", "水瓶座", "处女座", "射手座", "狮子座", "天秤座", "金牛座",
            "天蝎座", "双鱼座", "摩羯座", "双子座", "巨蟹座"};
    private String time[] = {"today", "tomorrow"};
    private Spinner spinner1, spinner2;
    private ArrayAdapter<String> arrayAdapter1, arrayAdapter2;
    private View view;
    private Button btn_query;
    private String encodeStr;
    private String type;
    private TextView tv_all_main, tv_health_main, tv_love_main, tv_money_main, tv_work_main,
            tv_fortune_main, tv_color_main, tv_num_main, tv_today_content, tv_today;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.constellation_fragment, null);
        initview();  //初始化控件
        tv_today.setText("今天概述");
        arrayAdapter1 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, start);
        arrayAdapter2 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, time);
        spinner1.setAdapter(arrayAdapter1);
        spinner2.setAdapter(arrayAdapter2);
        setListener();//设置下拉菜单监听
        requestConstellation("http://web.juhe.cn:8080/constellation/getAll?consName=%E7%99%BD%E7%BE%8A%E5%BA%A7&type=today&key=543f3616fe4204803942f8be0c881b6c");
        return view;
    }

    public void setListener() {
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    encodeStr = URLEncoder.encode(start[position], "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = time[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://web.juhe.cn:8080/constellation/getAll" + "?consName=" + encodeStr + "&type=" + type + "&key=543f3616fe4204803942f8be0c881b6c";
                Log.d("aaa", url);
                requestConstellation(url);
            }
        });
    }


    /*
    * 请求网络
    * */
    public void requestConstellation(String url) {
        HttpUtil.sendOkHttpRequest(url, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(),"加载失败",Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Response response) throws IOException {
                final String text = response.body().string();
                final Constellation constellation = Utility.parseJsonWithGson2(text);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv_all_main.setText(constellation.getAll());
                        tv_health_main.setText(constellation.getHealth());
                        tv_love_main.setText(constellation.getLove());
                        tv_money_main.setText(constellation.getMoney());
                        tv_work_main.setText(constellation.getWork());
                        tv_color_main.setText(constellation.getColor());
                        tv_num_main.setText(String.valueOf(constellation.getNumber()));
                        tv_fortune_main.setText(constellation.getQFriend());
                        tv_today_content.setText(constellation.getSummary());
                    }
                });
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (type.equals("today")) {
                            tv_today.setText("今天概述");
                        } else {
                            tv_today.setText("明天概述");
                        }
                    }
                });
            }
        });
    }

    public void initview() {
        spinner1 = view.findViewById(R.id.spinner1);
        spinner2 = view.findViewById(R.id.spinner2);
        btn_query = view.findViewById(R.id.btn_query);
        tv_all_main = view.findViewById(R.id.tv_all_main);
        tv_health_main = view.findViewById(R.id.tv_health_main);
        tv_love_main = view.findViewById(R.id.tv_love_main);
        tv_money_main = view.findViewById(R.id.tv_money_main);
        tv_work_main = view.findViewById(R.id.tv_work_main);
        tv_today_content = view.findViewById(R.id.tv_today_content);
        tv_color_main = view.findViewById(R.id.tv_color_main);
        tv_fortune_main = view.findViewById(R.id.tv_fortune_main);
        tv_num_main = view.findViewById(R.id.tv_num_main);
        tv_today = view.findViewById(R.id.tv_today);
    }
}
