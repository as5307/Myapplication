package com.example.myapplication.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.myapplication.R;
import com.example.myapplication.view.RadarView;

public class RadarFragment extends Fragment implements View.OnClickListener{
    private RadarView radar_view;
    private Button bt_scanning;
    private Button bt_stop;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.radar_view,null);
        radar_view=view.findViewById(R.id.radar_view);
        bt_scanning=view.findViewById(R.id.bt_scanning);
        bt_stop=view.findViewById(R.id.bt_stop);
        bt_scanning.setOnClickListener(this);
        bt_stop.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_scanning:
                Log.d("TAG","11111");
                radar_view.start();
                break;
            case R.id.bt_stop:
                Log.d("TAG","2222");
                radar_view.stop();
                break;
        }
    }
}
