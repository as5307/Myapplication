package com.example.myapplication.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.myapplication.Adapter.NewsAdapter;
import com.example.myapplication.R;
import com.example.myapplication.Title;
import com.example.myapplication.gson.News;
import com.example.myapplication.unti.HttpUtil;
import com.example.myapplication.unti.NetworkUtil;
import com.example.myapplication.unti.TimeUtil;
import com.example.myapplication.unti.Utility;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class NewFagment extends Fragment {
    private RecyclerView recycler_view;
    private List<Title> list = new ArrayList<>();
    private String address = "http://v.juhe.cn/toutiao/index?type=%E5%A4%B4%E6%9D%A1&key=de863d0cc0b86f160a2ee47571137cf7";
    private NewsAdapter newsAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Handler handler;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_fragment, null);
        requestNew();
        Log.d("aaa", "333333333");
        recycler_view = view.findViewById(R.id.recycler_view);
        swipeRefreshLayout = view.findViewById(R.id.refreshLayout);
        setswipeRefreshLayout();
        recycler_view.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        return view;
    }

    /*
    上拉刷新刷新
     */

    public void setswipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("aaa", "sssssss");
                boolean networkOK = NetworkUtil.checkInternetConnection(getActivity());
                if (!networkOK) {
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    swipeRefreshLayout.setRefreshing(true);
                    requestNew();
                }
            }
        });
    }


    /*
    请求数据
     */
    public void requestNew() {
        Log.d("aaa", "1111111111");
        HttpUtil.sendOkHttpRequest(address, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.d("TAG", "请求成功");
                final String text = response.body().string();
                Log.d("TAG", text);
                final News news = Utility.parseJsonWithGson(text);
                final News.ResultBean resultBean = news.result;
                final int code = news.error_code;
                if (code == 0) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            list.clear();
                            for (News.ResultBean.DataBean dataBean : resultBean.data) {
                                String time = TimeUtil.getTimeFormatText(dataBean.date);
                                Title title = new Title(time, dataBean.title, dataBean.thumbnail_pic_s, dataBean.author_name, dataBean.url);
                                list.add(title);
                            }
                            initAdapter(); //初始化适配器
                        }
                    });

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(NewFagment.this.getActivity(), "错误数据返回", Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }
        });
    }

    public void initAdapter() {
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        newsAdapter = new NewsAdapter(getActivity(), list);
        recycler_view.setAdapter(newsAdapter);
    }
}