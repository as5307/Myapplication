package com.example.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;



public class WebActivity extends AppCompatActivity {
    private WebView web_view;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);
        web_view=findViewById(R.id.web_view);

        WebSettings settings=web_view.getSettings();
        settings.setJavaScriptEnabled(true);
        String web=getIntent().getStringExtra("data");
        web_view.loadUrl(web);
    }
}
