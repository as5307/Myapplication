package com.example.myapplication.unti;

import com.example.myapplication.gson.Constellation;
import com.example.myapplication.gson.News;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 解析JSON的工具类
 */

public class Utility {
    public static News parseJsonWithGson(final String requestText){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create();
        return gson.fromJson(requestText, News.class);
    }
    public static Constellation parseJsonWithGson2(final String requestText){
        Gson gson = new Gson();
        return gson.fromJson(requestText, Constellation.class);
    }
}
