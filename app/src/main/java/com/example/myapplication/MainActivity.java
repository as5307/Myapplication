package com.example.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.myapplication.Fragment.ConstellationFragment;
import com.example.myapplication.Fragment.NewFagment;

import com.example.myapplication.Fragment.RadarFragment;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private String tabs[]={"新闻","星座运势","其他"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioGroup=findViewById(R.id.radiogroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_news:
                        SwitchFragmentSupport(R.id.fl_content,tabs[0]);
                        break;
                    case R.id.rb_weather:
                        SwitchFragmentSupport(R.id.fl_content,tabs[1]);
                        break;
                    case R.id.rb_other:
                        SwitchFragmentSupport(R.id.fl_content,tabs[2]);
                        break;
                }
        }
        });
        RadioButton radioButton= (RadioButton) radioGroup.getChildAt(0);
        radioButton.toggle();
    }

    public void SwitchFragmentSupport(int containerId, String tag){
        FragmentManager fragmentManager=getSupportFragmentManager();
        Fragment destFragment =fragmentManager.findFragmentByTag(tag);
        if (destFragment==null) {
            if (tag.equals(tabs[0])) destFragment = new NewFagment();
            if (tag.equals(tabs[1])) destFragment = new ConstellationFragment();
            if (tag.equals(tabs[2])) destFragment = new RadarFragment();
        }
        FragmentTransaction ft=fragmentManager.beginTransaction();
        ft.replace(containerId,destFragment,tag);
        ft.addToBackStack(null);
        ft.commit();
    }
}
