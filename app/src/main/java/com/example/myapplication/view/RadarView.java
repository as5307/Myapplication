package com.example.myapplication.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;



public class RadarView extends View {
    private Paint mPanintLine,mPaninCircle,mPaintPonints;
    private int w,h;
    private Matrix matrix;
    private int start=0;
    private int delayMillls=1;
    private int angle;

    private Handler handler=new Handler();
    private boolean isScanning=false;
    private Runnable runnable=new Runnable() {
        @Override
        public void run() {
            matrix=new Matrix();
            if (start>=360){
                start=0;
            }
            angle=delayMillls*start;
            if (isScanning){
                start=start+1;
                matrix.postRotate(start,w/2,h/3);
            }else {
                angle=delayMillls*start;
                matrix.postRotate(start,w/2,h/3);
            }
            RadarView.this.invalidate();
            handler.postDelayed(runnable,1);
        }
    };
    public RadarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initview();
        w=context.getResources().getDisplayMetrics().widthPixels;
        h=context.getResources().getDisplayMetrics().heightPixels;
        handler.post(runnable);
    }
    private void initview(){
        mPanintLine=new Paint();
        mPanintLine.setColor(Color.WHITE);
        mPanintLine.setAntiAlias(true);
        mPanintLine.setStyle(Paint.Style.STROKE);

        mPaninCircle=new Paint();
        mPaninCircle.setColor(Color.BLACK);
        mPaninCircle.setAntiAlias(true);

        mPaintPonints=new Paint();
        mPaintPonints.setStyle(Paint.Style.FILL);
        mPaintPonints.setColor(Color.BLUE);
        mPaninCircle.setAntiAlias(true);


    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
       drawstart(canvas);
    }
    public void start(){
        isScanning=true;
    }
    public void stop(){
        isScanning=false;
    }
    public void drawstart( Canvas canvas){
        canvas.drawCircle(w/2,h/3,w/3,mPanintLine);
        canvas.drawCircle(w/2,h/3,w/4,mPanintLine);
        canvas.drawCircle(w/2,h/3,w/5,mPanintLine);
        canvas.drawCircle(w/2,h/3,w/6,mPanintLine);
        Shader shader=new SweepGradient(w/2,h/3,Color.TRANSPARENT,Color.parseColor("#aaaaaaaa"));
        mPaninCircle.setShader(shader);
        canvas.concat(matrix);
        canvas.drawCircle(w/2,h/3,w/3,mPaninCircle);
    }
}
