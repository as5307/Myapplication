package com.example.myapplication;

public class Title {
    private String data;
    private String title;
    private String pciurle;
    private String author_name;
    private String url;

    public  Title(String data,String title,String pciurle,String author_name,String url){
        this.data=data;
        this.title=title;
        this.url=url;
        this.author_name=author_name;
        this.pciurle=pciurle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPciurle() {
        return pciurle;
    }

    public void setPciurle(String pciurle) {
        this.pciurle = pciurle;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

}
