 package com.example.myapplication.gson;

 import java.util.Date;
 import java.util.List;

 /**
  * 新闻
  */
 public class News {

     public String reason;
     public ResultBean result;
     public int error_code;

     public String getReason() {
         return reason;
     }

     public void setReason(String reason) {
         this.reason = reason;
     }

     public ResultBean getResult() {
         return result;
     }

     public void setResult(ResultBean result) {
         this.result = result;
     }

     public int getError_code() {
         return error_code;
     }

     public void setError_code(int error_code) {
         this.error_code = error_code;
     }

     public static class ResultBean {


         public String stat;
         public List<DataBean> data;

         public String getStat() {
             return stat;
         }

         public void setStat(String stat) {
             this.stat = stat;
         }

         public List<DataBean> getData() {
             return data;
         }

         public void setData(List<DataBean> data) {
             this.data = data;
         }

         public static class DataBean {
             /**
              * uniquekey : fed76396757e98af045afea6a9510451
              * title : 美人计 | 杨紫关晓彤欧阳娜娜，都逼我找“别人家自拍”学是吧？
              * date : 2018-12-16 19:29
              * category : 头条
              * author_name : 时尚COSMO
              * url : http://mini.eastday.com/mobile/181216192907502.html
              * thumbnail_pic_s : http://04imgmini.eastday.com/mobile/20181216/20181216192907_c7d9643cab3fd0e0626ecd0cd7e99b08_24_mwpm_03200403.jpg
              * thumbnail_pic_s02 : http://04imgmini.eastday.com/mobile/20181216/20181216192907_c7d9643cab3fd0e0626ecd0cd7e99b08_2_mwpm_03200403.jpg
              * thumbnail_pic_s03 : http://04imgmini.eastday.com/mobile/20181216/20181216192907_c7d9643cab3fd0e0626ecd0cd7e99b08_3_mwpm_03200403.jpg
              */

             public String uniquekey;
             public String title;
             public Date date;
             public String category;
             public String author_name;
             public String url;
             public String thumbnail_pic_s;
             public String thumbnail_pic_s02;
             public String thumbnail_pic_s03;

             public String getUniquekey() {
                 return uniquekey;
             }

             public void setUniquekey(String uniquekey) {
                 this.uniquekey = uniquekey;
             }

             public String getTitle() {
                 return title;
             }

             public void setTitle(String title) {
                 this.title = title;
             }

             public Date getDate() {
                 return date;
             }

             public void setDate(Date date) {
                 this.date = date;
             }

             public String getCategory() {
                 return category;
             }

             public void setCategory(String category) {
                 this.category = category;
             }

             public String getAuthor_name() {
                 return author_name;
             }

             public void setAuthor_name(String author_name) {
                 this.author_name = author_name;
             }

             public String getUrl() {
                 return url;
             }

             public void setUrl(String url) {
                 this.url = url;
             }

             public String getThumbnail_pic_s() {
                 return thumbnail_pic_s;
             }

             public void setThumbnail_pic_s(String thumbnail_pic_s) {
                 this.thumbnail_pic_s = thumbnail_pic_s;
             }

             public String getThumbnail_pic_s02() {
                 return thumbnail_pic_s02;
             }

             public void setThumbnail_pic_s02(String thumbnail_pic_s02) {
                 this.thumbnail_pic_s02 = thumbnail_pic_s02;
             }

             public String getThumbnail_pic_s03() {
                 return thumbnail_pic_s03;
             }

             public void setThumbnail_pic_s03(String thumbnail_pic_s03) {
                 this.thumbnail_pic_s03 = thumbnail_pic_s03;
             }
         }
     }
 }
